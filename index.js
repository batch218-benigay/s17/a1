/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	//first function here:

	function printUserInfo(){
		let fullName = prompt("Please enter your Fullname: ");
		let age = prompt("Please enter your age: ");
		let location = prompt("Please enter your location:");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}

	printUserInfo();

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function showFaveBands(){
		let firstBand = "Paramore";
		let secondBand = "Disturbed";
		let thirdBand = "Linkin Park";
		let fourthBand = "System of A Down";
		let fifthBand = "Third-eye Blind";

		console.log("1. "+ firstBand);
		console.log("2. "+ secondBand);
		console.log("3. "+thirdBand);
		console.log("4. "+fourthBand);
		console.log("5. "+fifthBand);
	};

	console.log ("Favorite Bands: ");
	showFaveBands();


/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function showFaveMovies(){
		let movie1 = "1. Psycho";
		let movie2 = "2. Rebecca";
		let movie3 = "3. Strangers on a Train";
		let movie4 = "4. Rear Window";
		let movie5 = "5. The 39 Steps";

		console.log(movie1 + "\n Rotten Tomatoes Rating: 96%");
		console.log(movie2 + "\n Rotten Tomatoes Rating: 98%");
		console.log(movie3 + "\n Rotten Tomatoes Rating: 98%");
		console.log(movie4 + "\n Rotten Tomatoes Rating: 98%");
		console.log(movie5 + "\n Rotten Tomatoes Rating: 96%");
	}

	console.log("Favorite Movies of all time:");
	showFaveMovies();

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();